# Pokedex

## Pods usados

- Alamofire para networking.
- AlamofireObjectMapper para mapear las respuestas.
- Kingfisher para descargar y cachear las imagenes.

## Notas

- En el *PokedexHomeViewController*, la idea es inyectar el viewModel como una dependencia, solo para este ejercicio decidí dejarlo asi por cuestiones de tiempo.
