//
//  Segues.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation

enum Segues {
	
	enum Home {
		static let showPokemonInfo = "showPokemonInfo"
	}
	
}
