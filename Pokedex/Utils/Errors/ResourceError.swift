//
//  ResourceError.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation

enum ResourceError: Error {
	case notFound
}
