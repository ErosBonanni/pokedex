//
//  UIViewController+Alert.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import UIKit

extension UIViewController {
	
	func showAlert(title: String, message: String) {
		
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
		let action = UIAlertAction(title: "OK", style: .default, handler: nil)
		alertController.addAction(action)
		self.present(alertController, animated: true)
		
	}
	
}
