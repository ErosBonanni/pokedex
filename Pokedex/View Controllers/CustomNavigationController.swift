//
//  CustomNavigationController.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		removeNavigationBarShadow()
		removeNavigationBarBackground()
	}
	
	func removeNavigationBarBackground() {
		navigationBar.backgroundColor = .clear
		navigationBar.isTranslucent = false
	}
	
	func removeNavigationBarShadow() {
		navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
		navigationBar.shadowImage = UIImage()
	}
	
}
