//
//  PokedexHomeViewController.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import UIKit

class PokedexHomeViewController: UIViewController {
	
	// MARK: - Properties
	
	let cellId = "cellId"
	
	var viewModel: PokedexHomeViewModel {
		let pokemonApi = PokemonAPI()
		return PokedexHomeViewModelClass(pokedexService: pokemonApi)
	}
	
	var pokemons: [Pokemon] = [] {
		didSet {
			pokemonsCollectionView.reloadData()
		}
	}
	
	// MARK: - Outlets
	
	@IBOutlet weak var loadingView: UIActivityIndicatorView!
	@IBOutlet weak var pokemonsCollectionView: UICollectionView!

	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupCollectionView()
		setupIcon()
		getPokemons()
	}
	
	// MARK: - Setup
	
	func setupIcon() {
		let image = UIImage(named: "Pokeball")
		let imageView = UIImageView(image: image)
		navigationItem.titleView = imageView
	}
	
	func setupCollectionView() {
		pokemonsCollectionView.delegate = self
		pokemonsCollectionView.dataSource = self
		let cellNib = UINib(nibName: "PokemonHomeCell", bundle: nil)
		pokemonsCollectionView.register(cellNib, forCellWithReuseIdentifier: cellId)
	}
	
	func getPokemons() {
		viewModel.getPokemons { (pokemons, error) in
			if let _ = error {
				self.showError()
			} else {
				guard let pokemons = pokemons else { return }
				self.pokemons = pokemons
			}
		}
	}
	
	func showError() {
		showAlert(title: "Error", message: "No hemos podido obtener los pokemons :(")
	}
	
}

extension PokedexHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return pokemons.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PokemonHomeCell
		let pokemon = pokemons[indexPath.item]
		cell.configure(with: pokemon)
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: 160, height: 180)
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		performSegue(withIdentifier: Segues.Home.showPokemonInfo, sender: indexPath.item)
	}
	
}

extension PokedexHomeViewController {
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		
		if segue.identifier == Segues.Home.showPokemonInfo {
			guard let destination = segue.destination as? PokemonInfoViewController else { return }
			guard let index = sender as? Int else { return }
			let pokemon = pokemons[index]
			destination.pokemon = pokemon
		}
		
	}
	
}
