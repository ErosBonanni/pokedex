//
//  PokemonInfoViewController.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import UIKit

class PokemonInfoViewController: UIViewController {
	
	// MARK: - Properties
	
	var pokemon: Pokemon!
	
	// MARK: - Outlets
	
	@IBOutlet weak var pokemonImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var typesLabel: UILabel!
	@IBOutlet weak var weightLabel: UILabel!
	@IBOutlet weak var heightLabel: UILabel!
	@IBOutlet weak var candyNameLabel: UILabel!
	@IBOutlet weak var candyCountLabel: UILabel!
	@IBOutlet weak var weaknessesLabel: UILabel!
	@IBOutlet weak var evolutionsLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configure()
		setupIcon()
	}
	
	func configure() {
		
		let viewModel = PokemonViewModel(pokemon)
		nameLabel.text = viewModel.name
		typesLabel.text = viewModel.types
		heightLabel.text = viewModel.height
		weightLabel.text = viewModel.weight
		candyNameLabel.text = viewModel.candyName
		candyCountLabel.text = viewModel.candyCount
		weaknessesLabel.text = viewModel.weaknesses
		evolutionsLabel.text = viewModel.evolutions
		
		if let url = viewModel.imageUrl {
			pokemonImageView.kf.indicatorType = .activity
			pokemonImageView.kf.setImage(with: url)
		}

	}
	
	func setupIcon() {
		let image = UIImage(named: "Pokeball")
		let imageView = UIImageView(image: image)
		navigationItem.titleView = imageView
	}
	
}
