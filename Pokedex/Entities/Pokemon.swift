//
//  Pokemon.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation
import ObjectMapper

struct Pokemon {
	
	var id: Int?
	var num: String?
	var name: String?
	var img: String?
	var type: [String]?
	var height: String?
	var weight: String?
	var candy: String?
	var candyCount: Int?
	var egg: String?
	var spawnChance: Float?
	var avgSpawns: Int?
	var spawnTime: String?
	var multipliers: [Float]?
	var weaknesses: [String]?
	var nextEvolution: [PokemonEvolution]?
	
}

extension Pokemon: Mappable {
	
	init?(map: Map) {}
	
	mutating func mapping(map: Map) {
		id <- map["id"]
		num <- map["num"]
		name <- map["name"]
		img <- map["img"]
		type <- map["type"]
		height <- map["height"]
		weight <- map["weight"]
		candy <- map["candy"]
		candyCount <- map["candy_count"]
		egg <- map["egg"]
		spawnChance <- map["spawn_chance"]
		avgSpawns <- map["avg_spawns"]
		spawnTime <- map["spawn_time"]
		multipliers <- map["multipliers"]
		weaknesses <- map["weaknesses"]
		nextEvolution <- map["next_evolution"]
	}
	
}
