//
//  PokemonEvolution.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation
import ObjectMapper

struct PokemonEvolution {
	var num: String?
	var name: String?
}

extension PokemonEvolution: Mappable {
	
	init?(map: Map) {}
	
	mutating func mapping(map: Map) {
		num <- map["num"]
		name <- map["name"]
	}
	
}
