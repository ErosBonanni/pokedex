//
//  PokemonResponse.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation
import ObjectMapper

struct PokemonResponse {
	var pokemons: [Pokemon]?
}

extension PokemonResponse: Mappable {
	
	init?(map: Map) {}
	
	mutating func mapping(map: Map) {
		pokemons <- map["pokemon"]
	}
	
}
