//
//  PokedexService.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol PokedexService {
	
	func getPokemons(completion: @escaping ( ([Pokemon]?, Error?) -> () ))
	
}

class PokemonAPI: PokedexService {
	
	let baseUrl = "https://raw.githubusercontent.com/"
	
	func getPokemons(completion: @escaping (([Pokemon]?, Error?) -> ())) {
		
		let url = baseUrl + "Biuni/PokemonGO-Pokedex/master/pokedex.json"
		
		Alamofire
			.request(url)
			.validate()
			.responseObject { (response: DataResponse<PokemonResponse>) in
				
				switch response.result {
				case .success(let pokemonsResponse):
					guard let pokemons = pokemonsResponse.pokemons?.compactMap({$0})
					else {
						completion(nil, ResourceError.notFound)
						return
					}
					completion(pokemons, nil)
					
				case .failure(let error):
					completion(nil, error)
				}
				
		}
	}
	
}
