//
//  PokemonViewModel.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation

protocol PokemonViewModelProtocol {
	var height: String { get }
	var weight: String { get }
	var weaknesses: String { get }
	var types: String { get }
	var candyName: String { get }
	var candyCount: String { get }
	var name: String { get }
	var evolutions: String { get }
	var imageUrl: URL? { get }
}

class PokemonViewModel: PokemonViewModelProtocol {
	
	private var pokemon: Pokemon
	
	init(_ pokemon: Pokemon) {
		self.pokemon = pokemon
	}
	
	var name: String {
		return pokemon.name ?? ""
	}
	
	var weaknesses: String {
		return pokemon.weaknesses?.joined(separator: ", ") ?? ""
	}
	
	var height: String {
		return pokemon.height ?? ""
	}
	
	var weight: String {
		return pokemon.weight ?? ""
	}
	
	var candyName: String {
		return pokemon.candy ?? ""
	}
	
	var candyCount: String {
		return pokemon.candyCount?.description ?? "None"
	}
	
	var types: String {
		return pokemon.type?.joined(separator: ", ") ?? ""
	}
	
	var imageUrl: URL? {
		guard let imageUrlString = pokemon.img, let url = URL(string: imageUrlString) else { return nil }
		return url
	}
	
	var evolutions: String {
		guard let nextEvolutions = pokemon.nextEvolution.map ({ $0 })
			else { return "This pokemon doesn't have next evolutions." }
		return nextEvolutions.compactMap { $0.name }.joined(separator: ", ")
	}
	
}
