//
//  PokedexHomeViewModel.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import Foundation

protocol PokedexHomeViewModel {
	
	func getPokemons(completion: @escaping ( ([Pokemon]?, Error?) -> ()))
	
}

class PokedexHomeViewModelClass: PokedexHomeViewModel {
	
	var pokedexService: PokedexService
	
	init(pokedexService: PokedexService) {
		self.pokedexService = pokedexService
	}
	
	func getPokemons(completion: @escaping (([Pokemon]?, Error?) -> ())) {
		pokedexService.getPokemons(completion: completion)
	}
	
}
