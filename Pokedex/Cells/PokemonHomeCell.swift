//
//  PokemonHomeCell.swift
//  Pokedex
//
//  Created by Eros Bonanni on 29/10/2018.
//  Copyright © 2018 ErosBonanni. All rights reserved.
//

import UIKit
import Kingfisher

class PokemonHomeCell: UICollectionViewCell {
	
	// MARK: - Outlets
	
	@IBOutlet weak var pokemonImageView: UIImageView!
	@IBOutlet weak var pokemonNameLabel: UILabel!
	
	func configure(with pokemon: Pokemon) {
		pokemonNameLabel.text = pokemon.name
		
		guard let imageUrlString = pokemon.img, let url = URL(string: imageUrlString) else { return }
		pokemonImageView.kf.indicatorType = .activity
		pokemonImageView.kf.setImage(with: url)
	}
	
}
